/// <reference types="cypress" />


beforeEach('Cookie setup', ()=>{
// Megfelelő cookie kimásolása az oldalról nagyjából félóránként
// Ha nem frissítjük a cookie-t akkor tele spammolja get requesttel az oldalt
    cy.setCookie('accessToken','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI4MyIsInJvbGVzIjpbIkFDQ09VTlRfTUFOQUdFUl9BRE1JTiIsIkRFVklDRV9NQU5BR0VSX0FETUlOIiwiUFJPSkVDVF9JTlZFTlRPUllfQURNSU4iLCJTS0lMTE1BUF9BRE1JTiJdLCJzdWIiOiI4MyIsImlhdCI6MTY1NDcxMzA1MSwiZXhwIjoxNjU1MzE3ODUxfQ.ASGsprB8USbSapfs7JHgCeVFmBVcm-hGkK0xbxcOEIcoaF8MCqYh1vmEJTFSsSTyFuXRCaqSr0conZm8UkHL0A')
})


/*
beforeEach('Eyes mindenkori megnyitása', ()=>{
  cy.eyesOpen({
    appName: 'Device Manager',
    testName: 'Random Teszt'
    

  });
})


afterEach('Eyes mindenkori bezárása', ()=>{
  cy.eyesColse();
})
*/



describe('Sima cypress teszt kezdésnek', ()=>{
    beforeEach('Visit site',() => {
        cy.visit('http://device-manager.dev.intra.attrecto.com')
        cy.wait(1000)
    })

    it('A Device Manager felirat megfelelően megjelent',()=>{
        cy.get('.app-title > .mb-0')
          .contains('Device Manager')
    })


    it('Főoldal check', ()=>{
      cy.eyesOpen({
          appName: 'Device Manager',
          testName: 'Főloldal check',

      });
      cy.eyesCheckWindow();
      cy.eyesClose();
  })

    it('Telefon & tablet check', ()=>{
      cy.get('.desktop > .drawer-inner > :nth-child(2) > [href="/mobiles-tablets"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Telefon & tablet oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();


    })

    it('Laptop & számítógép check', ()=>{
      cy.get('.desktop > .drawer-inner > :nth-child(2) > [href="/computers"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Laptop & számítógép oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Felhasználók check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/users"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Felhasználók oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Cégek check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/companies"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Cégek oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Partnerek check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/partners"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Partnerek oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Szervíz bejegyzések check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/service-tickets"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Szervíz bejegyzések oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Tartozék típusok check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/accessory-types"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Tartozék típusok oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Csatlakozó típusok check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/connector-types"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Csatlakozó típusok oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Eszköz kategóriák check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/device-categories"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Eszköz kategóriák oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Rendszer paraméterek check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/system-parameters"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Rendszer paraméterek oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })

    it('Importálás check', ()=>{
      cy.get('.desktop > .drawer-inner > .side-nav-item-wrapper > [href="/admin/import"]')
        .click()

      cy.eyesOpen({
        appName: 'Device Manager',
        testName: 'Importálás oldal'
      });

      cy.eyesCheckWindow();
      cy.eyesClose();
      
    })
    
})

